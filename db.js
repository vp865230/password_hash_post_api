import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

const mongoDbconn = async() => {
    try {
        const conn = await mongoose.connect('mongodb://localhost:27017/user',{
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        console.log(`MongoDb Connected ${conn.connection.host}`)
    }catch (err) {
        console.log(err)
    }
};

const userSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    dob: {
        type: String,
        require: true,
    },
    location: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    cpassword: {
        type: String,
        require: true
    }
});

userSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
    
        const salt = await bcrypt.genSalt(12);
        this.password = await bcrypt.hash(this.password,salt);
        this.cpassword = await bcrypt.hash(this.cpassword,salt);
    } 
    next();
    
});

const User = mongoose.model('User',userSchema);

export { mongoDbconn, User };