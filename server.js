import express from 'express';
import { mongoDbconn, User } from './db.js'

mongoDbconn();

const app = express();

app.use(express.json());

app.get('/',(req,res) => {
    res.send('Api is running')
});

app.post('/register', async (req,res) => {

    const { name, email, dob, location, password, cpassword} = req.body;

    if (!name || !email || !dob || !location || !password || !cpassword) {
        return res.status(422).json({error: "Plz fill properly"})
    }

    try {
        // Additional feature that i create
        const userExist = await User.findOne({ email: email});

        if (userExist) {
            return res.status(422).json({ error: "Email already Exist"});
        } else if (password !== cpassword) {
            return res.status(422).json({ error: "Password are not match"})
        } else {

            const user = new User({ name,email,dob,location,password,cpassword});
    
            await user.save();
    
            res.status(201).json({ message: "user registered successfuly"});
        }
    } catch (err) {
        console.log(err)
    }
});


app.listen(5000, () => {
    console.log('Server Running Port 5000')
})